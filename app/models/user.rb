class User < ActiveRecord::Base
  attr_accessible :access_token, :expires, :name, :provider, :refresh_token, :uid
  validates_uniqueness_of :uid, :scope => :provider
end
